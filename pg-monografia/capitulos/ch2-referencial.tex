% ==============================================================================
% TCC - Thiago Martinho
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Referencial Teórico}
\label{sec-referencial}
Este capítulo apresenta as principais bases teóricas para o desenvolvimento do Resgate – o Sistema de Despacho de Ambulância Automatizado, sendo elas: especificação de requisitos (Seção~\ref{sec-ref-teo-especificacao}), projeto de sistemas e implementação (Seção~\ref{sec-ref-teo-projeto-impl}) e o método \textit{FrameWeb} (Seção~\ref{sec-ref-teo-frameweb}).

%%% Início de seção. %%%
\section{Especificação de Requisitos}
\label{sec-ref-teo-especificacao}

A especificação de requisitos compreende a primeira etapa do processo de desenvolvimento de um \textit{software}. Nesta etapa são definidas as funcionalidades que o sistema deve conter e os critérios de qualidade sob os quais o sistema deverá operar. É caracterizada por um esforço conjunto entre clientes, usuários e especialistas de domínio a fim de traçar uma linha conjunta de entendimento entre todas as partes envolvidas a respeito de como um \textit{software} se comportará e de como será utilizado pelo usuário. Trata-se de uma atividade complexa que não se resume somente a perguntar às pessoas o que elas desejam, mas sim analisar cuidadosamente a organização, o domínio da aplicação e os processos de negócio no qual o sistema será utilizado \cite{sommerville1998requirements}.

Durante essa fase, as partes envolvidas fazem uma análise profunda nas funções que o \textit{software} deverá conter, especificando assim, os requisitos do sistema. Para requisitos existem diversas definições na literatura, valendo destacar:

\begin{itemize}
	\item Requisitos de um sistema são descrições dos serviços que devem ser fornecidos por esse sistema e as suas restrições operacionais \cite{sommerville2003engenharia};
	\item Um requisito de um sistema é a característica do sistema ou a descrição de algo que o sistema é capaz de realizar para atingir seus objetivos \cite{pfleeger2004engenharia};
	\item Um requisito é alguma coisa que o produto tem de fazer ou uma qualidade que ele precisa apresentar \cite{robertson2006mastering}.
\end{itemize}

Com base nessas e em outras definições, pode-se dizer que os requisitos de um sistema incluem especificações dos serviços que o sistema deve prover, restrições sob as quais ele deve operar, propriedades gerais do sistema e restrições que devem ser satisfeitas no seu processo de desenvolvimento \cite{falboEngReq}. Os requisitos de \textit{software} de um sistema podem ser divididos em funcionais e não funcionais.

Requisitos funcionais são declarações de serviços que o sistema deve prover, descrevendo o que o sistema deve fazer \cite{sommerville2003engenharia}. Já os não funcionais, descrevem restrições sobre os serviços ou funções oferecidas pelo sistema \cite{sommerville2003engenharia}, as quais limitam as opções para criar uma solução para o problema \cite{pfleeger2004engenharia}.
 
Um dos modelos criados durante a fase de requisitos, o modelo de casos de uso visa capturar e descrever as funcionalidades que um sistema deve prover para os atores que interagem com o mesmo, isto é, os usuários do sistema \cite{souza2007frameweb}. Com isso, o usuário tem a descrição passo a passo de todas as funcionalidades do sistema e de como pode prosseguir para executar determinada tarefa.

Os casos de uso cadastrais de baixa complexidade que envolvem as funcionalidades de criar, alterar, excluir e consulta, podem ser descritos na forma de tabela como proposto por \cite{falboEngReq}. Existem diferentes tipos de usuários no sistema, que são chamados de atores. Através dessa caracterização é possível identificar quais usuários podem e quais não podem executar determinada tarefa no sistema.

Ainda como produto dessa fase do desenvolvimento de um \textit{software}, a modelagem conceitual estrutural visa modelar de forma computacional os requisitos e os casos de uso identificados anteriormente. Por meio deste modelo, é possível identificar de forma clara os tipos de entidade e os tipos de relacionamentos presentes no sistema. O propósito da modelagem conceitual estrutural, segundo o paradigma orientado a objetos, é definir as classes, atributos e associações que são relevantes para tratar o problema a ser resolvido \cite{falboEngReq}.

%%% Início de seção. %%%
\section{Projeto de Sistemas e Implementação}
\label{sec-ref-teo-projeto-impl}

A fase de Projeto durante a especificação de um \textit{software} tem por objetivo definir e especificar uma solução a ser implementada \cite{falboProjSist}. É a fase onde retira-se da etapa de levantamento e especificação de requisitos o “o que” deve ser feito e busca o “como” deve ser feito. É nesta fase que são determinadas as tecnologias de implementação e de armazenamento que serão utilizadas durante o processo de desenvolvimento do sistema. Esta fase é decomposta em etapas, sendo que a primeira consiste em definir uma arquitetura de \textit{software}, seguido do detalhamento e projeto dos elementos da arquitetura e por fim o projeto detalhado.

A arquitetura de \textit{software} de um programa ou sistema computacional é a estrutura ou estruturas do sistema, que abrange os componentes de software, as propriedades externamente visíveis desses componentes e as relações entre eles \cite{pressman2005software}, ou seja, a etapa de arquitetura de \textit{software} tem como objetivo obter os relacionamentos dos componentes do sistema assim como identificar tais componentes. Ao final desta etapa, segue-se para o detalhamento que em \cite{falboProjSist} é definida como tendo o objetivo de projetar em um maior nível de detalhes cada um dos elementos estruturais definidos na arquitetura, o que envolve a decomposição de módulos em outros módulos menores. Com o resultado dessa etapa, já é possível identificar os primeiros módulos de implementação, onde no caso do paradigma orientado a objetos, seriam as classes. 

Por fim, o detalhamento do projeto tem por objetivo refinar e detalhar os elementos mais básicos da arquitetura do \textit{software}: as interfaces, os procedimentos e as estruturas de dados. Deve-se escrever como se dará a comunicação entre os componentes da arquitetura, a comunicação do sistema em desenvolvimento com outros sistemas e com as pessoas que irão utilizá-lo \cite{falboProjSist}.

Ao final da fase de Projeto de \textit{software}, tem-se a arquitetura na qual o sistema será montado juntamente com o projeto dos componentes do sistema. Esses componentes, como proposto em \cite{fowler2002patterns}, podem ser classificados da seguinte forma: interface com o usuário, lógica de negócio e persistência de dados. A interface com o usuário é o componente responsável por gerenciar todo o contato do usuário com o sistema, seja para receber dados quanto para apresentar dados. O componente de lógica de negócio é encarregado de aplicar todas as regras de negócios e requisitos do sistema. É uma camada intermediária de um sistema, que recebe os dados vindo da interface com o usuário e após aplicada a lógica de negócio repassa os dados para a camada seguinte. A persistência de dados tem como tarefa guardar os dados de forma a ser possível recuperá-los quando necessário em um sistema de banco de dados integrando com o sistema.


%%% Início de seção. %%%
\section{O método \textit{FrameWeb}}
\label{sec-ref-teo-frameweb}

O método \textit{FrameWeb} foi proposto por \cite{souza2007frameweb} e trata-se de um método com o intuito de auxiliar a fase de Projeto na construção de sistemas de informação para a \textit{web} que utilizem \textit{frameworks}, devido ao fato de serem propostos modelos de projeto que retratem mais fielmente a implementação do sistema. Este método assume que existem tipos determinados de \textit{frameworks} utilizados durante todo o resto do desenvolvimento do \textit{software}, que engloba a definição de uma arquitetura básica para o sistema e a implementação.

Mesmo sendo um método focado na etapa de Projeto de Sistemas, o \textit{FrameWeb} espera que toda as fases anteriores do desenvolvimento de \textit{software} tenham sido concluídas e que diagramas de casos de uso e de classes de domínio tenham sido construídos. A figura ~\ref{fig-processo-software} representa um processo de desenvolvimento de \textit{software} sugerido pelo \textit{FrameWeb}.

\begin{figure}[h]
	\centering
	\includegraphics{figuras/cap2/processo-software} 
	\caption{Um processo de desenvolvimento de software simples sugerido pelo \textit{FrameWeb}.}
	\label{fig-processo-software}
\end{figure}

De acordo com a especificação original do método, a fase de Projeto concentra as propostas principais do método: (i) definição de uma arquitetura padrão que divide o sistema em camadas, de modo a se integrar bem com os \textit{frameworks} utilizados; (ii) proposta de um conjunto de modelos de projeto que trazem conceitos utilizados pelos \textit{frameworks} para esta fase do processo por meio da criação de um perfil UML que faz com que os diagramas fiquem mais próximos da implementação \cite{souza2007frameweb}.

Como se trata de um método que tem como objetivo ser uma alternativa na fase de Projeto de Sistemas incentivando o uso de \textit{frameworks}, a parte da implementação se torna menos complicada. É possível simplificar o processo de desenvolvimento, teste e até implantação com a utilização dessas ferramentas tornando o processo de desenvolvimento do \textit{software} uma tarefa um pouco menos complexa.

Assim como proposto em \cite{fowler2002patterns} o \textit{FameWeb} define uma arquitetura de \textit{software} lógica baseada no padrão camada de serviço, sendo decomposto em três camadas diferentes: lógica de apresentação, lógica de negócio e lógica de acesso a dados conforme indicado na figura ~\ref{fig-arquitetura-padrao}.

\begin{figure}[h]
	\centering
	\includegraphics{figuras/cap2/arquitetura-padrao} 
	\caption{Arquitetura padrão para Sistema de Informação \textit{Web} baseada no padrão Camada de Serviço \cite{fowler2002patterns}.}
	\label{fig-arquitetura-padrao}
\end{figure}

A lógica de apresentação é a camada que engloba a parte de interação com o usuário. O pacote Visão contém páginas \textit{Web}, folhas de estilo, imagens, \textit{scripts} que executam do lado do cliente, modelos de documentos e outros arquivos relacionados exclusivamente com a exibição de informações ao usuário. O pacote Controle envolve classes de ação e outros arquivos relacionados ao Controlador Frontal \cite{souza2007frameweb}. Logo, toda a interação do usuário com o sistema é possível devido ao pacote de Visão e todo o gerenciamento dessas informações, tais como, a obtenção de informações inseridas pelo usuário e disponibilização de informações para o usuário, são de responsabilidade do pacote Controle.

Em seguida, a camada de lógica de negócio contém os pacotes de aplicação e de domínio. No pacote de domínio encontra-se todas as classes que foram identificadas ainda na fase de Análise de Requisitos como sendo do domínio do problema. Já o pacote de aplicação é responsável por aplicar a lógica de negócio do sistema, sendo a implementação da parte lógica dos casos de uso. Tal camada se relaciona diretamente com os elementos de domínio do problema e com a lógica de apresentação. Os dados recebidos pelos controladores são passados para a camada de lógica de negócio através do pacote de aplicação, que por sua vez devolve a computação realizada para o pacote de controle. A relação entre o pacote de Controle e Domínio é estereotipada como fraca porque não são feitas alterações em entidades de domínio persistentes: objetos de Domínio são utilizados na camada de apresentação apenas para exibição de seus dados ou como parâmetros nas evocações de métodos entre um pacote e outro \cite{souza2007frameweb}.

Por fim, a camada de Lógica de Acesso a Dados possui apenas o pacote de Persistência que é responsável por gerenciar a persistência de dados do sistema. O método \textit{FrameWeb} assume a utilização de um \textit{framework} de mapeamento objeto/relacional para a lógica de persistência de dados através do padrão de projeto \textit{Data Access Object} (DAO) \cite{alur2003core}. Com isso, toda a computação realizada pelo pacote de aplicação que necessita ser armazenada é enviada para o pacote de persistência. Da mesma forma, se para realizar uma computação a aplicação necessite de algum dado que esteja persistido, tal dado é solicitado a camada de Lógica de Acesso a Dados. A relação fraca entre o pacote de Persistência e o pacote de Domínio se dá pelo fato de os elementos do domínio serem necessários para a persistência apenas para efetuar o mapeamento para o banco de dados relacional.

Ao final da fase de Projeto utilizando o método \textit{FrameWeb} é esperado que tenha como produto quatro modelos diferentes, a saber: Modelo de Domínio, Modelo de Persistência, Modelo de Navegação e Modelo de Aplicação. Tais modelos são extensões a UML propostos pelo método aqui discutido, com o intuito de representar mais detalhadamente os elementos considerados comuns para ferramentas \textit{Web}. Cada modelo visa representar as informações levantadas durante o processo de desenvolvimento para todas as camadas da arquitetura de software. Os dados representados por cada modelo, segundo a especificação do método \textit{FrameWeb} descrita por \cite{souza2007frameweb}, são:

\begin{itemize}
	\item Modelo de Domínio: digrama de classes da UML que representa os objetos de domínio do problema e seu mapeamento para a persistência em banco de dados relacional;
	\item Modelo de Persistência: diagrama de classes da UML que representa as classes DAO existentes, responsáveis pela persistência das instâncias das classes de domínio;
	\item Modelo de Navegação: diagrama de classe da UML que representa os diferentes componentes que formam a camada de Lógica de Apresentação, como páginas Web, formulários HTML e classes de ação;
	\item Modelo de Aplicação: diagrama de classes da UML que representa as classes de serviço, que são responsáveis pela codificação dos casos de uso, e suas dependências.
\end{itemize}