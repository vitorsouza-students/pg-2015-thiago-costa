/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.controller;

import br.ufes.inf.nemo.resgate.data.application.ManageHospitalService;
import br.ufes.inf.nemo.resgate.data.domain.Hospital;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Thiago
 */
@Named
@SessionScoped
public class ManageHospitalController extends CrudController<Hospital> {

    @EJB
    private ManageHospitalService manageHospitalService;

    public ManageHospitalController() {
        viewPath = "/manageHospital/";
        bundleName = "msgs";
    }

    @Override
    protected CrudService<Hospital> getCrudService() {
        return manageHospitalService;
    }

    @Override
    protected Hospital createNewEntity() {
        return new Hospital();
    }

    @Override
    protected void initFilters() {
    }
}
