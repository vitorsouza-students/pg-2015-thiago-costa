/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.application;

import br.ufes.inf.nemo.resgate.dispatch.domain.AmbulanceSettings;
import br.ufes.inf.nemo.resgate.dispatch.persistence.AmbulanceSettingsDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Thiago
 */
@Stateless
public class ManageAmbulanceSettingsServiceBean extends CrudServiceBean<AmbulanceSettings> implements ManageAmbulanceSettingsService {

    @EJB
    private AmbulanceSettingsDAO ambulanceSettingsDAO;

    @Override
    protected AmbulanceSettings createNewEntity() {
        return new AmbulanceSettings();
    }

    @Override
    public BaseDAO<AmbulanceSettings> getDAO() {
        return ambulanceSettingsDAO;
    }

}
