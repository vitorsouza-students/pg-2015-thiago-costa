/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.persistence;

import br.ufes.inf.nemo.resgate.data.domain.Hospital;
import br.ufes.inf.nemo.resgate.data.domain.Hospital_;
import br.ufes.inf.nemo.resgate.util.Util;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Thiago
 */
@Stateless
public class HospitalJPADAO extends BaseJPADAO<Hospital> implements HospitalDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<Hospital> getDomainClass() {
        return Hospital.class;
    }

    @Override
    public List<Hospital> retrieveAllActiveHospitals() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Hospital> cq = cb.createQuery(Hospital.class);
        Root<Hospital> root = cq.from(Hospital.class);
        Predicate activeHospitals = cb.equal(root.get(Hospital_.active), Util.ACTIVE);
        cq.where(activeHospitals);
        List<Hospital> hospitals = null;
        try {
            hospitals = entityManager.createQuery(cq).getResultList();
        } catch (RuntimeException e) {
        }
        return hospitals;
    }
}
