/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.application;

import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.resgate.data.domain.Station;
import br.ufes.inf.nemo.resgate.data.persistence.AmbulanceDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author thiago
 */
@Stateless
public class ManageAmbulanceServiceBean extends CrudServiceBean<Ambulance> implements ManageAmbulanceService {

    @EJB
    private AmbulanceDAO ambulanceDAO;

    @Override
    protected Ambulance createNewEntity() {
        return new Ambulance();
    }

    @Override
    public BaseDAO<Ambulance> getDAO() {
        return ambulanceDAO;
    }

    @Override
    public void create(Ambulance entity) {
        Station station = entity.getStation();
        entity.setLatitude(station.getAddress().getLatitude());
        entity.setLongitude(station.getAddress().getLongitude());
        super.create(entity); //To change body of generated methods, choose Tools | Templates.
    }
}
