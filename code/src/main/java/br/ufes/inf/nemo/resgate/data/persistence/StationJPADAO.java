/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.persistence;

import br.ufes.inf.nemo.resgate.data.domain.Station;
import br.ufes.inf.nemo.resgate.data.domain.Station_;
import br.ufes.inf.nemo.resgate.util.Util;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Thiago
 */
@Stateless
public class StationJPADAO extends BaseJPADAO<Station> implements StationDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<Station> getDomainClass() {
        return Station.class;
    }

    @Override
    public List<Station> retrieveAllActiveStations() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Station> cq = cb.createQuery(Station.class);
        Root<Station> root = cq.from(Station.class);
        Predicate activeStation = cb.equal(root.get(Station_.active), Util.ACTIVE);
        cq.where(activeStation);
        List<Station> equipments = null;
        try {
            equipments = entityManager.createQuery(cq).getResultList();
        } catch (RuntimeException e) {
        }
        return equipments;
    }

}
