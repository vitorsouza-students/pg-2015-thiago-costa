/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

/**
 *
 * @author thiago
 */
@Entity
public class Station extends PersistentObjectSupport implements Comparable<Station> {

    @Size(max = 150)
    private String name;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Address address;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "station", fetch = FetchType.EAGER)
    private Set<Employee> employees;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "station", fetch = FetchType.EAGER)
    private Set<Ambulance> ambulances;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "station", fetch = FetchType.EAGER)
    private Set<Equipment> equipments;

    private boolean active;

    public Station() {
        employees = new HashSet<>();
        ambulances = new HashSet<>();
        equipments = new HashSet<>();
        address = new Address();
        active = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    public Set<Ambulance> getAmbulances() {
        return ambulances;
    }

    public void setAmbulances(Set<Ambulance> ambulances) {
        this.ambulances = ambulances;
    }

    public Set<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(Set<Equipment> equipments) {
        this.equipments = equipments;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int compareTo(Station o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
