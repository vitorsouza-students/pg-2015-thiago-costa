/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.controller;

import br.ufes.inf.nemo.resgate.data.application.ManageSessionService;
import br.ufes.inf.nemo.resgate.data.domain.Employee;
import br.ufes.inf.nemo.resgate.data.domain.StaffFunction;
import br.ufes.inf.nemo.util.ejb3.controller.JSFController;
import java.util.Date;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Thiago
 */
@Named
@SessionScoped
public class ManageSessionController extends JSFController {

    private static final long serialVersionUID = 1L;

    @EJB
    private ManageSessionService manageSessionService;

    private String username;

    private String password;

    private Employee currentUser;

    public ManageSessionController() {
        bundleName = "msgs";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return currentUser != null;
    }

    public boolean beforeLogin() {
        currentUser = null;
        return true;
    }

    public Employee getCurrentUser() {
        return currentUser;
    }

    public Date getNow() {
        return new Date(System.currentTimeMillis());
    }

    public Date getSessionExpirationTime() {
        Date expTime = null;

        // Attempts to retrieve this information from the external context.
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session != null) {
            long expTimeMillis = session.getLastAccessedTime() + session.getMaxInactiveInterval() * 1000;
            expTime = new Date(expTimeMillis);
        }

        // If it could not be retrieved from the external context, use default of 30 minutes.
        if (expTime == null) {
            expTime = new Date(System.currentTimeMillis() + 30 * 60000);
        }

        return expTime;
    }

    public String login() {
        try {
            currentUser = manageSessionService.login(username, password);
        } catch (Exception e) {
            addGlobalI18nMessage("msgs", FacesMessage.SEVERITY_ERROR, "error.login.summary", "error.login.detail");
            return null;
        }
        return "/manageDispatch/list.xhtml";
    }

    public String logout() {
        username = "";
        password = "";
        currentUser = null;
        return "/index.faces";
    }

    public boolean isAdmin() {
        return currentUser.getStaffFunctions().contains(StaffFunction.ADMIN);
    }

    public boolean isAttendant() {
        return currentUser.getStaffFunctions().contains(StaffFunction.ATTENDANT);
    }

    public String getWelcome() {
        return getI18nMessage("msgs", "menu.welcome") + " " + currentUser.getName();
    }
}
