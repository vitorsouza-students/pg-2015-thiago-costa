/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.controller;

import br.ufes.inf.nemo.resgate.dispatch.application.ManageDispatchService;
import br.ufes.inf.nemo.resgate.dispatch.domain.Dispatch;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.resgate.util.Pair;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Thiago
 */
@Named
@SessionScoped
public class ManageDispatchController extends CrudController<Dispatch> {

    @EJB
    private ManageDispatchService manageDispatchService;

    private Incident incident;

    private List selectedAmbulancesToBeDispatched;

    public ManageDispatchController() {
        viewPath = "/manageDispatch/";
        bundleName = "msgs";

    }

    @Override
    protected CrudService<Dispatch> getCrudService() {
        return manageDispatchService;
    }

    @Override
    protected Dispatch createNewEntity() {
        return new Dispatch();
    }

    @Override
    protected void initFilters() {
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public Incident getIncident() {
        return incident;
    }

    public List<Incident> getIncidentsPendingDispatch() {
        return manageDispatchService.getIncidentsPendingDisptach();
    }

    public List<Pair> getAmbulanceDistanceToIncident() {
        return manageDispatchService.getAmbulancesByDistance(this.incident);
    }

    public void setSelectedAmbulancesToBeDispatched(List selectedAmbulancesToBeDispatched) {
        this.selectedAmbulancesToBeDispatched = selectedAmbulancesToBeDispatched;
    }

    public List getSelectedAmbulancesToBeDispatched() {
        return selectedAmbulancesToBeDispatched;
    }

    public String dispatch() {
        manageDispatchService.dispatchAmbulances(incident, selectedAmbulancesToBeDispatched);
        return "list.xhtml";
    }
}
