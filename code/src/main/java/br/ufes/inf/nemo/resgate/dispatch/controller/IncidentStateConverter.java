/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.controller;

import br.ufes.inf.nemo.resgate.dispatch.domain.IncidentState;
import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Thiago
 */
@FacesConverter(value = "incidentStateConverter")
public class IncidentStateConverter extends EnumConverter {

    public IncidentStateConverter() {
        super(IncidentState.class);
    }
}
