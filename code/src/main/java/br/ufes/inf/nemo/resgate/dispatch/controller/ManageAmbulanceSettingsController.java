/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.controller;

import br.ufes.inf.nemo.resgate.dispatch.application.ManageAmbulanceSettingsService;
import br.ufes.inf.nemo.resgate.dispatch.domain.AmbulanceSettings;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Thiago
 */
@Named
@SessionScoped
public class ManageAmbulanceSettingsController extends CrudController<AmbulanceSettings> {

    @EJB
    private ManageAmbulanceSettingsService manageAmbulanceSettingsService;

    public ManageAmbulanceSettingsController() {
        viewPath = "/manageAmbulance/";
        bundleName = "msgs";
    }

    @Override
    protected CrudService<AmbulanceSettings> getCrudService() {
        return manageAmbulanceSettingsService;
    }

    @Override
    protected AmbulanceSettings createNewEntity() {
        return new AmbulanceSettings();
    }

    @Override
    protected void initFilters() {
    }

}
