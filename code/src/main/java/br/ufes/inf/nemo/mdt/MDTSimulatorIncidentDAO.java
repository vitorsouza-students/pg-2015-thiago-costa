/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.mdt;

import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Thiago
 */
@Local
public interface MDTSimulatorIncidentDAO extends BaseDAO<Incident> {

    public Incident getIncidentByAmbulance(Ambulance ambulance);

}
