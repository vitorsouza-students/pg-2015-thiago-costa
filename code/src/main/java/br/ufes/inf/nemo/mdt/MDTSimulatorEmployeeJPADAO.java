/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.mdt;

import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.resgate.data.domain.Employee;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Thiago
 */
@Stateless
public class MDTSimulatorEmployeeJPADAO extends BaseJPADAO<Employee> implements MDTSimulatorEmployeeDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<Employee> getDomainClass() {
        return Employee.class;
    }

    @Override
    public void putEmployeesOnHold(Ambulance ambulance) {
        try {
            String jpql = "SELECT e FROM Employee e where e.ambulance = :ambulance";

            TypedQuery<Employee> query = entityManager.createQuery(jpql,
                    Employee.class).setParameter("ambulance", ambulance);
            Employee employee = query.getSingleResult();
            employee.setAmbulance(null);
            save(employee);
        } catch (Exception ex) {

        }
    }
}
