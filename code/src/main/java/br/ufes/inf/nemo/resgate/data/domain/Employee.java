/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

/**
 *
 * @author thiago
 */
@Entity
public class Employee extends PersistentObjectSupport implements Comparable<Employee> {

    @Size(max = 150)
    private String name;

    @Size(max = 10)
    private String password;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<StaffFunction> staffFunctions;

    @ManyToOne(fetch = FetchType.EAGER)
    private Ambulance ambulance;

    @ManyToOne(fetch = FetchType.EAGER)
    private Station station;

    private boolean active;

    public Employee() {
        staffFunctions = new HashSet<>();
        active = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<StaffFunction> getStaffFunctions() {
        return staffFunctions;
    }

    public void setStaffFunctions(Set<StaffFunction> staffFunctions) {
        this.staffFunctions = staffFunctions;
    }

    public Ambulance getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(Ambulance ambulance) {
        this.ambulance = ambulance;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    @Override
    public int compareTo(Employee o) {
        return this.name.compareTo(o.name);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
