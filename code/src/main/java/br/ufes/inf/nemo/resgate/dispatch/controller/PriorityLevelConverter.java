package br.ufes.inf.nemo.resgate.dispatch.controller;

import br.ufes.inf.nemo.resgate.dispatch.domain.PriorityLevel;
import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Thiago
 */
@FacesConverter(value = "priorityLevelConverter")
public class PriorityLevelConverter extends EnumConverter {

    public PriorityLevelConverter() {
        super(PriorityLevel.class);
    }
}
