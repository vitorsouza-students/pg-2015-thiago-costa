/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.domain;

import br.ufes.inf.nemo.resgate.data.domain.Address;
import br.ufes.inf.nemo.resgate.data.domain.Hospital;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Thiago
 */
@Entity
public class Incident extends PersistentObjectSupport implements Comparable<Incident> {

    @OneToMany(mappedBy = "incident", fetch = FetchType.EAGER)
    private Set<Dispatch> dispatches;

    @ManyToOne(fetch = FetchType.EAGER)
    private Hospital hospital;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Address address;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<AmbulanceSettings> ambulanceSettings;

    private PriorityLevel priorityLevel;

    private IncidentState incidentState;

    public Incident() {
        dispatches = new HashSet<>();
        hospital = new Hospital();
        address = new Address();
        ambulanceSettings = new HashSet<>();
        incidentState = IncidentState.OPEN;
    }

    public Set<Dispatch> getDispatches() {
        return dispatches;
    }

    public void setDispatches(Set<Dispatch> dispatches) {
        this.dispatches = dispatches;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<AmbulanceSettings> getAmbulanceSettings() {
        return ambulanceSettings;
    }

    public void setAmbulanceSettings(Set<AmbulanceSettings> ambulanceSettings) {
        this.ambulanceSettings = ambulanceSettings;
    }

    public PriorityLevel getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(PriorityLevel priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public IncidentState getIncidentState() {
        return incidentState;
    }

    public void setIncidentState(IncidentState incidentState) {
        this.incidentState = incidentState;
    }

    @Override
    public int compareTo(Incident o) {
        return 0;
    }

}
