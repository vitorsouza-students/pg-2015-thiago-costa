/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.mdt;

import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Thiago
 */
@Stateless
public class MDTSimulatorAmbulanceJPADAO extends BaseJPADAO<Ambulance> implements MDTSimulatorAmbulanceDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<Ambulance> getDomainClass() {
        return Ambulance.class;
    }

}
