/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.mdt;

import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Thiago
 */
@Stateless
public class MDTSimulatorIncidentJPADAO extends BaseJPADAO<Incident> implements MDTSimulatorIncidentDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<Incident> getDomainClass() {
        return Incident.class;
    }

    @Override
    public Incident getIncidentByAmbulance(Ambulance ambulance) {
        String jpql = "SELECT DISTINCT inc FROM Incident inc, Dispatch d where "
                + " d.ambulance = :ambulance and d.incident = inc";

        TypedQuery<Incident> query = entityManager.createQuery(jpql,
                Incident.class).setParameter("ambulance", ambulance);
        System.err.println(query.toString());
        Incident incident = query.getSingleResult();
        return incident;
    }

}
