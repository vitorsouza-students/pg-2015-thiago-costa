/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.application;

import br.ufes.inf.nemo.resgate.dispatch.domain.Dispatch;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.resgate.util.Pair;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Thiago
 */
@Local
public interface ManageDispatchService extends CrudService<Dispatch> {

    public List<Pair> getAmbulancesByDistance(Incident incident);

    public List<Incident> getIncidentsPendingDisptach();

    public void dispatchAmbulances(Incident incident, List<Pair> selectedAmbulancesToBeDispatched);

}
