/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.application;

import br.ufes.inf.nemo.resgate.dispatch.domain.EmergencyCall;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.resgate.dispatch.persistence.EmergencyCallDAO;
import br.ufes.inf.nemo.resgate.dispatch.persistence.IncidentDAO;
import br.ufes.inf.nemo.resgate.util.GoogleMapsUtil;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import com.google.maps.model.LatLng;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Thiago
 */
@Stateless
public class ManageEmergencyCallServiceBean extends CrudServiceBean<EmergencyCall> implements ManageEmergencyCallService {

    @EJB
    private EmergencyCallDAO emergencyCallDAO;

    @EJB
    private IncidentDAO incidentDAO;

    @Override
    protected EmergencyCall createNewEntity() {
        return new EmergencyCall();
    }

    @Override
    public BaseDAO<EmergencyCall> getDAO() {
        return emergencyCallDAO;
    }

    @Override
    public List<EmergencyCall> getSimilarEmergencyCalls(EmergencyCall emergencyCall) {
        return emergencyCallDAO.getSimilarCalls(emergencyCall.getDescription(), emergencyCall.getNumber());
    }

    @Override
    public void setSimilarCallIncident(EmergencyCall emergencyCall, Incident incident) {
        Incident inc = incidentDAO.retrieveById(incident.getId());
        emergencyCall.setIncident(inc);
    }

    @Override
    public void create(EmergencyCall entity) {
        String fullAddress = entity.getIncident().getAddress().getStreet() + "," + entity.getIncident().getAddress().getNeighborhood()
                + "," + entity.getIncident().getAddress().getCity() + " " + entity.getIncident().getAddress().getZipCode();
        LatLng latLng = GoogleMapsUtil.getLatLngFromAddress(fullAddress);
        if (latLng != null) {
            entity.getIncident().getAddress().setLatitude(latLng.lat);
            entity.getIncident().getAddress().setLongitude(latLng.lng);
        }
        super.create(entity); //To change body of generated methods, choose Tools | Templates.
    }
}
