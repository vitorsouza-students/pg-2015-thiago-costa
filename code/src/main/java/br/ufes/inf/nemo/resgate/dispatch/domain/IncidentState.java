/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.domain;

/**
 *
 * @author Thiago
 */
public enum IncidentState {

    OPEN, IN_ATTENDANCE, EN_ROUTE_TO_HOSPITAL, CLOSED;

}
