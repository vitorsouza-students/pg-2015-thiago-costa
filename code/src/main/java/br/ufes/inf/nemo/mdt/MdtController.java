/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.mdt;

import br.ufes.inf.nemo.resgate.data.application.ManageAmbulanceService;
import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.resgate.data.domain.AmbulanceState;
import br.ufes.inf.nemo.resgate.data.persistence.AmbulanceDAO;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.resgate.dispatch.domain.IncidentState;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import br.ufes.inf.nemo.util.ejb3.controller.PersistentObjectConverterFromId;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.convert.Converter;

/**
 *
 * @author Thiago
 */
@SessionScoped
@Named
public class MdtController extends CrudController<Ambulance> {

    @EJB
    private ManageAmbulanceService manageAmbulanceService;

    @EJB
    private AmbulanceDAO ambulanceDAO;

    @EJB
    private MDTSimulatorAmbulanceDAO mDTSimulatorAmbulanceDAO;

    @EJB
    private MDTSimulatorEmployeeDAO mDTSimulatorEmployeeDAO;

    @EJB
    private MDTSimulatorIncidentDAO mDTSimulatorIncidentDAO;

    @EJB
    private MDTSimulatorEquipmentDAO mDTSimulatorEquipmentDAO;

    private Ambulance ambulance;

    private List<Ambulance> ambulances;

    public MdtController() {
        viewPath = "/MDTSimulator/";
        bundleName = "msgs";
    }

    @Override
    protected CrudService<Ambulance> getCrudService() {
        return manageAmbulanceService;
    }

    @Override
    protected Ambulance createNewEntity() {
        return new Ambulance();
    }

    @Override
    protected void initFilters() {
    }

    public Ambulance getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(Ambulance ambulance) {
        this.ambulance = ambulance;
    }

    public Converter getAmbulanceConverter() {
        return new PersistentObjectConverterFromId<>(ambulanceDAO);
    }

    public List<Ambulance> getAmbulances() {
        if (ambulances == null) {
            ambulances = ambulanceDAO.retrieveAll();
        }
        return ambulances;
    }

    public void ambulanceLocalization() {
        mDTSimulatorAmbulanceDAO.save(ambulance);
    }

    public void confirmationIncidenteAttendance() {
        ambulance.setAmbulanceState(AmbulanceState.EN_ROUTE);
        mDTSimulatorAmbulanceDAO.save(ambulance);

        Incident incident = mDTSimulatorIncidentDAO.getIncidentByAmbulance(ambulance);
        incident.setIncidentState(IncidentState.IN_ATTENDANCE);
        mDTSimulatorIncidentDAO.save(incident);
    }

    public void enRouteToHospital() {
        ambulance.setAmbulanceState(AmbulanceState.EN_ROUTE);
        mDTSimulatorAmbulanceDAO.save(ambulance);

        Incident incident = mDTSimulatorIncidentDAO.getIncidentByAmbulance(ambulance);
        incident.setIncidentState(IncidentState.EN_ROUTE_TO_HOSPITAL);
        mDTSimulatorIncidentDAO.save(incident);
    }

    public void informAtHospital() {
        ambulance.setAmbulanceState(AmbulanceState.AT_HOSPITAL);
        mDTSimulatorAmbulanceDAO.save(ambulance);

        Incident incident = mDTSimulatorIncidentDAO.getIncidentByAmbulance(ambulance);
        incident.setIncidentState(IncidentState.EN_ROUTE_TO_HOSPITAL);
        mDTSimulatorIncidentDAO.save(incident);
    }

    public void informEnRouteToStation() {
        ambulance.setAmbulanceState(AmbulanceState.EN_ROUTE_TO_STATION);
        mDTSimulatorAmbulanceDAO.save(ambulance);

        Incident incident = mDTSimulatorIncidentDAO.getIncidentByAmbulance(ambulance);
        incident.setIncidentState(IncidentState.CLOSED);
        mDTSimulatorIncidentDAO.save(incident);
    }

    public void informOnHold() {
        ambulance.setAmbulanceState(AmbulanceState.AT_STATION);
        mDTSimulatorAmbulanceDAO.save(ambulance);
        mDTSimulatorEmployeeDAO.putEmployeesOnHold(ambulance);
        mDTSimulatorEquipmentDAO.putEquipmentOnHold(ambulance);
    }

    public void informProblem() {
        ambulance.setAmbulanceState(AmbulanceState.INACTIVE);
        mDTSimulatorAmbulanceDAO.save(ambulance);

        Incident incident = mDTSimulatorIncidentDAO.getIncidentByAmbulance(ambulance);
        incident.setIncidentState(IncidentState.OPEN);
        mDTSimulatorIncidentDAO.save(incident);
    }
}
