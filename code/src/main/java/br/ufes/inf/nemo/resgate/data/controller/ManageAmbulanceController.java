/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.controller;

import br.ufes.inf.nemo.resgate.data.application.ManageAmbulanceService;
import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author thiago
 */
@Named
@SessionScoped
public class ManageAmbulanceController extends CrudController<Ambulance> {

    @EJB
    private ManageAmbulanceService manageAmbulanceService;

    public ManageAmbulanceController() {
        viewPath = "/manageAmbulance/";
        bundleName = "msgs";
    }

    @Override
    protected CrudService<Ambulance> getCrudService() {
        return manageAmbulanceService;
    }

    @Override
    protected Ambulance createNewEntity() {
        return new Ambulance();
    }

    @Override
    protected void initFilters() {
    }

}
