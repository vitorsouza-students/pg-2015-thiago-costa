/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.persistence;

import br.ufes.inf.nemo.resgate.data.domain.Hospital;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Thiago
 */
@Local
public interface HospitalDAO extends BaseDAO<Hospital> {

    public List<Hospital> retrieveAllActiveHospitals();
}
