/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.persistence;

import br.ufes.inf.nemo.resgate.data.domain.Employee;
import br.ufes.inf.nemo.resgate.data.domain.Employee_;
import br.ufes.inf.nemo.resgate.data.domain.StaffFunction;
import br.ufes.inf.nemo.resgate.data.domain.Station;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Thiago
 */
@Stateless
public class EmployeeJPADAO extends BaseJPADAO<Employee> implements EmployeeDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<Employee> getDomainClass() {
        return Employee.class;
    }

    @Override
    public List<Employee> retriveAvailableEmployeesAtStation(Station station, List<StaffFunction> staffFunctions) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
        Root<Employee> root = cq.from(Employee.class);
        Predicate stationsPredicate = cb.equal(root.get(Employee_.station), station);
        Predicate ambulancePredicate = root.get(Employee_.ambulance).isNull();

        String jpql = "SELECT DISTINCT e FROM Employee e, IN (e.staffFunctions) e1 where e1 in (:searchList)"
                + " and e.ambulance is null and e.station = :station";

        TypedQuery<Employee> query = entityManager.createQuery(jpql,
                Employee.class).setParameter("searchList", staffFunctions).setParameter("station", station);
        List<Employee> employees = query.getResultList();

        return employees == null ? new ArrayList<>() : employees;
    }

    @Override
    public Employee retriveByName(String name) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
        Root<Employee> root = cq.from(Employee.class);
        Predicate where = cb.equal(root.get(Employee_.name), name);
        cq.where(where);
        Employee employee = null;
        try {
            employee = entityManager.createQuery(cq).getSingleResult();
        } catch (RuntimeException e) {
        }
        return employee;
    }
}
