/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.application;

import br.ufes.inf.nemo.resgate.data.domain.Address;
import br.ufes.inf.nemo.resgate.data.persistence.AddressDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Thiago
 */
@Stateless
public class ManageAddressServiceBean extends CrudServiceBean<Address> implements ManageAddressService {

    @EJB
    private AddressDAO addressDAO;

    @Override
    protected Address createNewEntity() {
        return new Address();
    }

    @Override
    public BaseDAO<Address> getDAO() {
        return addressDAO;
    }
}
