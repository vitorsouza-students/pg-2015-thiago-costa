/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.controller;

import br.ufes.inf.nemo.resgate.data.domain.EquipmentType;
import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Thiago
 */
@FacesConverter(value = "equipmentTypeConverter")
public class EquipmentTypeConverter extends EnumConverter {

    public EquipmentTypeConverter() {
        super(EquipmentType.class);
    }
}
