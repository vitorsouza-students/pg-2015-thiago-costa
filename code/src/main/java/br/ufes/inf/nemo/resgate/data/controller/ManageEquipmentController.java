/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.controller;

import br.ufes.inf.nemo.resgate.data.application.ManageEquipmentService;
import br.ufes.inf.nemo.resgate.data.domain.Equipment;
import br.ufes.inf.nemo.resgate.data.domain.EquipmentType;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Thiago
 */
@Named
@SessionScoped
public class ManageEquipmentController extends CrudController<Equipment> {

    @EJB
    private ManageEquipmentService manageEquipmentService;

    private List<EquipmentType> equipmentTypes;

    public ManageEquipmentController() {
        viewPath = "/manageEquipment/";
        bundleName = "msgs";
    }

    @Override
    protected CrudService<Equipment> getCrudService() {
        return manageEquipmentService;
    }

    @Override
    protected Equipment createNewEntity() {
        return new Equipment();
    }

    @Override
    protected void initFilters() {
    }

    public List<EquipmentType> getEquipmentTypes() {
        if (equipmentTypes == null) {
            equipmentTypes = new ArrayList<>();
            EquipmentType[] equipmentType = EquipmentType.values();
            equipmentTypes.addAll(Arrays.asList(equipmentType));
        }

        return equipmentTypes;
    }

}
