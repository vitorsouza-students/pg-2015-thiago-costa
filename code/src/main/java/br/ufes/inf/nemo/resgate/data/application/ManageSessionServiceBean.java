/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.application;

import br.ufes.inf.nemo.resgate.data.domain.Employee;
import br.ufes.inf.nemo.resgate.data.persistence.EmployeeDAO;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.ejb.EJB;

/**
 *
 * @author Thiago
 */
@SessionScoped
@Stateful
public class ManageSessionServiceBean implements ManageSessionService {

    @EJB
    private EmployeeDAO employeeDAO;

    private Employee currentUser;

    @Override
    public Employee login(String username, String password) throws Exception {
        try {
            Employee user = employeeDAO.retriveByName(username);

            // Checks if the passwords match.
            String pwd = user.getPassword();
            if (user.getPassword().equals(pwd)) {
                currentUser = user;
            } else {
                throw new Exception();
            }

            return currentUser;
        } catch (Exception e) {
            throw e;
        }
    }
}
