/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.util;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.DistanceMatrixRow;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.google.maps.model.Unit;

/**
 *
 * @author Thiago
 */
public class GoogleMapsUtil {

    private static final GeoApiContext contextServerKey = new GeoApiContext().setApiKey("AIzaSyD2GPGCw68x9uuqe57ly1jDBuOog3XoLu8");

    public static Long getDistanceMatrix(LatLng[] origns, LatLng[] destinations) {
        Long distance = null;
        try {
            DistanceMatrixApiRequest distanceMatrixApiRequest = DistanceMatrixApi.newRequest(contextServerKey).origins(origns).destinations(destinations).units(Unit.METRIC);
            DistanceMatrixRow[] distanceMatrixRows = distanceMatrixApiRequest.awaitIgnoreError().rows;
            if (distanceMatrixRows.length > 0 && distanceMatrixRows[0].elements.length > 0) {
                distance = distanceMatrixRows[0].elements[0].distance.inMeters;
            }
        } catch (Exception e) {

        }
        return distance;
    }

    public static LatLng getLatLngFromAddress(String address) {
        LatLng latLng = null;
        try {
            GeocodingResult[] results = GeocodingApi.newRequest(contextServerKey).address(address).await();
            if (results.length > 0) {
                latLng = results[0].geometry.location;
            }
        } catch (Exception e) {

        }
        return latLng;
    }
}
