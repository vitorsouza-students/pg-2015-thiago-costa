/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.controller;

import br.ufes.inf.nemo.resgate.dispatch.application.ManageIncidentService;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.resgate.util.GoogleMapsUtil;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import com.google.maps.model.LatLng;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Thiago
 */
@Named
@SessionScoped
public class ManageIncidentController extends CrudController<Incident> {

    @EJB
    private ManageIncidentService manageIncidentService;

    public ManageIncidentController() {
        viewPath = "/manageIncident/";
        bundleName = "msgs";
    }

    @Override
    protected CrudService<Incident> getCrudService() {
        return manageIncidentService;
    }

    @Override
    protected Incident createNewEntity() {
        return new Incident();
    }

    @Override
    protected void initFilters() {

    }

}
