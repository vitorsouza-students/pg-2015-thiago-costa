/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.application;

import br.ufes.inf.nemo.resgate.data.domain.Station;
import br.ufes.inf.nemo.resgate.data.persistence.StationDAO;
import br.ufes.inf.nemo.resgate.util.GoogleMapsUtil;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import com.google.maps.model.LatLng;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Thiago
 */
@Stateless
public class ManageStationServiceBean extends CrudServiceBean<Station> implements ManageStationService {

    @EJB
    private StationDAO stationDAO;

    @Override
    protected Station createNewEntity() {
        return new Station();
    }

    @Override
    public BaseDAO<Station> getDAO() {
        return stationDAO;
    }

    @Override
    public void create(Station entity) {
        String fullAddress = entity.getAddress().getStreet() + "," + entity.getAddress().getNeighborhood()
                + "," + entity.getAddress().getCity() + " " + entity.getAddress().getZipCode();
        LatLng latLng = GoogleMapsUtil.getLatLngFromAddress(fullAddress);
        if (latLng != null) {
            entity.getAddress().setLatitude(latLng.lat);
            entity.getAddress().setLongitude(latLng.lng);
        }
        super.create(entity); //To change body of generated methods, choose Tools | Templates.
    }
}
