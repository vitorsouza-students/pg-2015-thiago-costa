/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.application;

import br.ufes.inf.nemo.resgate.data.domain.Address;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import javax.ejb.Local;

/**
 *
 * @author Thiago
 */
@Local
public interface ManageAddressService extends CrudService<Address> {

}
