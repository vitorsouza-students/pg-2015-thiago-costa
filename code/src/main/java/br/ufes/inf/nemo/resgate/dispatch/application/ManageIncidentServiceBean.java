/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.application;

import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.resgate.dispatch.persistence.IncidentDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Thiago
 */
@Stateless
public class ManageIncidentServiceBean extends CrudServiceBean<Incident> implements ManageIncidentService {

    @EJB
    private IncidentDAO incidentDAO;

    @Override
    protected Incident createNewEntity() {
        return new Incident();
    }

    @Override
    public BaseDAO<Incident> getDAO() {
        return incidentDAO;
    }
}
