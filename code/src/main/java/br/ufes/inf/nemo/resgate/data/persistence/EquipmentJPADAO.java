/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.persistence;

import br.ufes.inf.nemo.resgate.data.domain.Equipment;
import br.ufes.inf.nemo.resgate.data.domain.Equipment_;
import br.ufes.inf.nemo.resgate.data.domain.EquipmentType;
import br.ufes.inf.nemo.resgate.data.domain.Station;
import br.ufes.inf.nemo.resgate.util.Util;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Thiago
 */
@Stateless
public class EquipmentJPADAO extends BaseJPADAO<Equipment> implements EquipmentDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<Equipment> getDomainClass() {
        return Equipment.class;
    }

    @Override
    public List<Equipment> retriveAvailableEquipmentsAtStation(Station station, List<EquipmentType> equipmentTypes) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Equipment> cq = cb.createQuery(Equipment.class);
        Root<Equipment> root = cq.from(Equipment.class);
        Predicate stationsPredicate = cb.equal(root.get(Equipment_.station), station);
        Predicate ambulancePredicate = root.get(Equipment_.ambulance).isNull();
        Predicate activeEquipments = cb.equal(root.get(Equipment_.active), Util.ACTIVE);
        Predicate p = root.get(Equipment_.equipmentType).in(equipmentTypes);
        cq.where(stationsPredicate, ambulancePredicate, activeEquipments, p);
        List<Equipment> equipments = null;
        try {
            equipments = entityManager.createQuery(cq).getResultList();
        } catch (RuntimeException e) {
        }
        return equipments;
    }
}
