/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.application;

import br.ufes.inf.nemo.resgate.data.domain.Equipment;
import br.ufes.inf.nemo.resgate.data.persistence.EquipmentDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Thiago
 */
@Stateless
public class ManageEquipmentServiceBean extends CrudServiceBean<Equipment> implements ManageEquipmentService {

    @EJB
    private EquipmentDAO equipmentDAO;

    @Override
    protected Equipment createNewEntity() {
        return new Equipment();
    }

    @Override
    public BaseDAO<Equipment> getDAO() {
        return equipmentDAO;
    }

}
