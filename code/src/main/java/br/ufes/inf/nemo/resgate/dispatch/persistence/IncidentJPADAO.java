/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.persistence;

import br.ufes.inf.nemo.resgate.dispatch.domain.Dispatch;
import br.ufes.inf.nemo.resgate.dispatch.domain.Dispatch_;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.resgate.dispatch.domain.IncidentState;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident_;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Thiago
 */
@Stateless
public class IncidentJPADAO extends BaseJPADAO<Incident> implements IncidentDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<Incident> getDomainClass() {
        return Incident.class;
    }

    @Override
    public List<Incident> retriveIncidentsPendingDisptach() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Incident> cq = cb.createQuery(Incident.class);
        Root<Incident> root = cq.from(Incident.class);
        cq.where(cb.equal(root.get(Incident_.incidentState), IncidentState.OPEN));
        List<Incident> incidents = null;
        try {
            incidents = entityManager.createQuery(cq).getResultList();
        } catch (RuntimeException e) {
        }
        return incidents;
    }
}
