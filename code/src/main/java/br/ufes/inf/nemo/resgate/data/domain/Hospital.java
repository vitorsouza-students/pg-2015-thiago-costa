/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

/**
 *
 * @author thiago
 */
@Entity
public class Hospital extends PersistentObjectSupport {

    @Size(max = 150)
    private String name;

    @ManyToOne(cascade = CascadeType.MERGE , fetch = FetchType.EAGER)
    private Address address;

    private boolean active;

    public Hospital() {
        address = new Address();
        active = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return name;
    }
}
