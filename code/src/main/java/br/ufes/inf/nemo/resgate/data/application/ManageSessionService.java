/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.application;

import br.ufes.inf.nemo.resgate.data.domain.Employee;
import java.io.Serializable;
import javax.ejb.Local;

/**
 *
 * @author Thiago
 */
@Local
public interface ManageSessionService extends Serializable {

    Employee login(String username, String password) throws Exception;

}
