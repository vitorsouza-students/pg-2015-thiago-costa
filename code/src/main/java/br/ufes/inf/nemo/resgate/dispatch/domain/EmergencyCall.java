/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.domain;

import br.ufes.inf.nemo.resgate.data.domain.Employee;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Thiago
 */
@Entity
public class EmergencyCall extends PersistentObjectSupport implements Comparable<EmergencyCall> {

    private String number;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;

    private String description;

    private String callerName;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Employee employee;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Incident incident;

    public EmergencyCall() {
        dateTime = new Date();
        number = "+5527987654321";
        employee = new Employee();
        incident = new Incident();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCallerName() {
        return callerName;
    }

    public void setCallerName(String callerName) {
        this.callerName = callerName;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    @Override
    public int compareTo(EmergencyCall o) {
        return this.dateTime.compareTo(o.dateTime);
    }

}
