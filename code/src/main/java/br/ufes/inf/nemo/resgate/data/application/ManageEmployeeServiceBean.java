/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.application;

import br.ufes.inf.nemo.resgate.data.domain.Employee;
import br.ufes.inf.nemo.resgate.data.persistence.EmployeeDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Thiago
 */
@Stateless
public class ManageEmployeeServiceBean extends CrudServiceBean<Employee> implements ManageEmployeeService {

    @EJB
    private EmployeeDAO employeeDAO;

    @Override
    protected Employee createNewEntity() {
        return new Employee();
    }

    @Override
    public BaseDAO<Employee> getDAO() {
        return employeeDAO;
    }
}
