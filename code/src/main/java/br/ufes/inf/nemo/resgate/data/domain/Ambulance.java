/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.domain;

import br.ufes.inf.nemo.resgate.dispatch.domain.Dispatch;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

/**
 *
 * @author thiago
 */
@Entity
public class Ambulance extends PersistentObjectSupport implements Comparable<Ambulance> {

    @Size(max = 10)
    private String plate;

    private AmbulanceState ambulanceState;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ambulance", fetch = FetchType.EAGER)
    private Set<Equipment> equipments;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ambulance", fetch = FetchType.EAGER)
    private Set<Employee> employees;

    @ManyToOne
    private Station station;

    private double latitude;

    private double longitude;

    public Ambulance() {
        equipments = new HashSet<>();
        employees = new HashSet<>();
        ambulanceState = AmbulanceState.AT_STATION;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public AmbulanceState getAmbulanceState() {
        return ambulanceState;
    }

    public void setAmbulanceState(AmbulanceState ambulanceState) {
        this.ambulanceState = ambulanceState;
    }

    public Set<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(Set<Equipment> equipments) {
        this.equipments = equipments;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int compareTo(Ambulance o) {
        return this.plate.compareTo(o.plate);
    }

    @Override
    public String toString() {
        return plate;
    }
}
