/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

/**
 *
 * @author thiago
 */
@Entity
public class Equipment extends PersistentObjectSupport implements Comparable<Equipment> {

    @Size(max = 150)
    private String identifier;

    private EquipmentType equipmentType;

    @ManyToOne(fetch = FetchType.EAGER)
    private Station station;

    @ManyToOne(fetch = FetchType.EAGER)
    private Ambulance ambulance;

    private boolean active;

    public Equipment() {
        active = true;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Ambulance getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(Ambulance ambulance) {
        this.ambulance = ambulance;
    }

    @Override
    public int compareTo(Equipment o) {
        return this.identifier.compareTo(o.identifier);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
