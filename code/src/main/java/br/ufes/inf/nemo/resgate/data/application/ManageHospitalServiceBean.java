/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.application;

import br.ufes.inf.nemo.resgate.data.domain.Hospital;
import br.ufes.inf.nemo.resgate.data.persistence.AddressDAO;
import br.ufes.inf.nemo.resgate.data.persistence.HospitalDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Thiago
 */
@Stateless
public class ManageHospitalServiceBean extends CrudServiceBean<Hospital> implements ManageHospitalService {

    @EJB
    private HospitalDAO hospitalDAO;

    @EJB
    private AddressDAO addressDAO;

    @Override
    protected Hospital createNewEntity() {
        return new Hospital();
    }

    @Override
    public BaseDAO<Hospital> getDAO() {
        return hospitalDAO;
    }

    @Override
    public void create(Hospital entity) {
        addressDAO.save(entity.getAddress());
        super.create(entity); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Hospital entity) {
        addressDAO.delete(entity.getAddress());
        super.delete(entity); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Hospital entity) {
        addressDAO.save(entity.getAddress());
        super.update(entity); //To change body of generated methods, choose Tools | Templates.
    }

}
