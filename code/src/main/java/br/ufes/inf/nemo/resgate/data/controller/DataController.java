/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.controller;

import br.ufes.inf.nemo.resgate.data.domain.Station;
import br.ufes.inf.nemo.resgate.data.persistence.StationDAO;
import br.ufes.inf.nemo.util.ejb3.controller.PersistentObjectConverterFromId;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.convert.Converter;
import javax.inject.Named;

/**
 *
 * @author Thiago
 */
@Named
@ApplicationScoped
public class DataController implements Serializable {

    @EJB
    private StationDAO stationDAO;

    public List<Station> getActiveStations() {
        return stationDAO.retrieveAllActiveStations();
    }

    public Converter getStationConverter() {
        return new PersistentObjectConverterFromId<>(stationDAO);
    }
}
