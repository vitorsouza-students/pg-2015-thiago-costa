/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.controller;

import br.ufes.inf.nemo.resgate.data.controller.ManageSessionController;
import br.ufes.inf.nemo.resgate.dispatch.application.ManageEmergencyCallService;
import br.ufes.inf.nemo.resgate.dispatch.domain.AmbulanceSettings;
import br.ufes.inf.nemo.resgate.dispatch.domain.EmergencyCall;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Thiago
 */
@Named
@SessionScoped
public class ManageEmergencyCallController extends CrudController<EmergencyCall> {

    @EJB
    private ManageEmergencyCallService manageEmergencyCallService;

    @Inject
    private ManageSessionController manageSessionController;

    private AmbulanceSettings ambulanceSettings = new AmbulanceSettings();

    private List<EmergencyCall> emergencyCalls;

    private EmergencyCall selectedSimilarEmergencyCall;

    public ManageEmergencyCallController() {
        viewPath = "/manageEmergencyCall/";
        bundleName = "msgs";
    }

    @Override
    protected CrudService<EmergencyCall> getCrudService() {
        return manageEmergencyCallService;
    }

    @Override
    protected EmergencyCall createNewEntity() {
        EmergencyCall ec = new EmergencyCall();
        ec.setEmployee(manageSessionController.getCurrentUser());
        return ec;
    }

    @Override
    protected void initFilters() {
    }

    public List<EmergencyCall> getSimilarCalls() {
        return emergencyCalls;
    }

    public void checkForSimilarCalls() {
        emergencyCalls = manageEmergencyCallService.getSimilarEmergencyCalls(this.getSelectedEntity());
    }

    public void insertAmbulanceSettings() {
        if (!ambulanceSettings.getEquipmentTypes().isEmpty() && !ambulanceSettings.getStaffFunctions().isEmpty()) {
            this.getSelectedEntity().getIncident().getAmbulanceSettings().add(ambulanceSettings);
            ambulanceSettings = new AmbulanceSettings();
        }
    }

    public AmbulanceSettings getAmbulanceSettings() {
        return ambulanceSettings;
    }

    public void setAmbulanceSettings(AmbulanceSettings ambulanceSettings) {
        this.ambulanceSettings = ambulanceSettings;
    }

    public EmergencyCall getSelectedSimilarEmergencyCall() {
        return selectedSimilarEmergencyCall;
    }

    public void setSelectedSimilarEmergencyCall(EmergencyCall selectedSimilarEmergencyCall) {
        this.selectedSimilarEmergencyCall = selectedSimilarEmergencyCall;
    }

    @Override
    public String save() {
        emergencyCalls = null;
        return super.save(); //To change body of generated methods, choose Tools | Templates.
    }

    public String confirmSimilarEmergencyCalls() {
        save();
        emergencyCalls = null;
        manageEmergencyCallService.setSimilarCallIncident(this.selectedEntity, this.selectedSimilarEmergencyCall.getIncident());
        return "/manageDispatch/list.xhtml";
    }
}
