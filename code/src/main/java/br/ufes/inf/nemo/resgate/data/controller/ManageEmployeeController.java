/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.controller;

import br.ufes.inf.nemo.resgate.data.application.ManageEmployeeService;
import br.ufes.inf.nemo.resgate.data.domain.Employee;
import br.ufes.inf.nemo.resgate.data.domain.StaffFunction;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Thiago
 */
@Named
@SessionScoped
public class ManageEmployeeController extends CrudController<Employee> {

    @EJB
    private ManageEmployeeService manageEmployeeService;

    private List<StaffFunction> staffFunctions;

    public ManageEmployeeController() {
        viewPath = "/manageEmployee/";
        bundleName = "msgs";
    }

    @Override
    protected CrudService<Employee> getCrudService() {
        return manageEmployeeService;
    }

    @Override
    protected Employee createNewEntity() {
        return new Employee();
    }

    @Override
    protected void initFilters() {

    }

    public List<StaffFunction> getStaffFunctions() {
        if (staffFunctions == null) {
            staffFunctions = new ArrayList<>();
            StaffFunction[] staffFuncs = StaffFunction.values();
            staffFunctions.addAll(Arrays.asList(staffFuncs));
        }

        return staffFunctions;
    }
}
