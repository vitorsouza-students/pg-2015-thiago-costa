/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.controller;

import br.ufes.inf.nemo.resgate.data.application.ManageStationService;
import br.ufes.inf.nemo.resgate.data.domain.Station;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Thiago
 */
@Named
@SessionScoped
public class ManageStationController extends CrudController<Station> {

    @EJB
    private ManageStationService manageStationService;

    public ManageStationController() {
        viewPath = "/manageStation/";
        bundleName = "msgs";
    }

    @Override
    protected CrudService<Station> getCrudService() {
        return manageStationService;
    }

    @Override
    protected Station createNewEntity() {
        return new Station();
    }

    @Override
    protected void initFilters() {
    }

}
