/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.application;

import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.resgate.data.domain.AmbulanceState;
import br.ufes.inf.nemo.resgate.data.domain.Employee;
import br.ufes.inf.nemo.resgate.data.domain.Equipment;
import br.ufes.inf.nemo.resgate.data.domain.EquipmentType;
import br.ufes.inf.nemo.resgate.data.domain.StaffFunction;
import br.ufes.inf.nemo.resgate.data.domain.Station;
import br.ufes.inf.nemo.resgate.data.persistence.AmbulanceDAO;
import br.ufes.inf.nemo.resgate.data.persistence.EmployeeDAO;
import br.ufes.inf.nemo.resgate.data.persistence.EquipmentDAO;
import br.ufes.inf.nemo.resgate.dispatch.domain.AmbulanceSettings;
import br.ufes.inf.nemo.resgate.dispatch.domain.Dispatch;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.resgate.dispatch.domain.IncidentState;
import br.ufes.inf.nemo.resgate.dispatch.persistence.DispatchDAO;
import br.ufes.inf.nemo.resgate.dispatch.persistence.IncidentDAO;
import br.ufes.inf.nemo.resgate.util.GoogleMapsUtil;
import br.ufes.inf.nemo.resgate.util.Pair;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import com.google.maps.model.LatLng;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Thiago
 */
@Stateless
public class ManageDispatchServiceBean extends CrudServiceBean<Dispatch> implements ManageDispatchService {

    @EJB
    private DispatchDAO dispatchDAO;

    @EJB
    private AmbulanceDAO ambulanceDAO;

    @EJB
    private IncidentDAO incidentDAO;

    @EJB
    private EmployeeDAO employeeDAO;

    @EJB
    private EquipmentDAO equipmentDAO;

    @Override
    protected Dispatch createNewEntity() {
        return new Dispatch();
    }

    @Override
    public BaseDAO<Dispatch> getDAO() {
        return dispatchDAO;
    }

    @Override
    public List<Pair> getAmbulancesByDistance(Incident incident) {
        List<Ambulance> ambulances = ambulanceDAO.retrieveAllAmbulancesAvailableToDispatch();
        if (ambulances == null) {
            return null;
        }
        LatLng[] dest = new LatLng[1];
        LatLng latLongDest = new LatLng(incident.getAddress().getLatitude(), incident.getAddress().getLongitude());
        dest[0] = latLongDest;
        List<Pair> ambulanceIdDistance = new ArrayList<>();
        for (Ambulance ambulance : ambulances) {
            LatLng[] origin = new LatLng[1];
            LatLng latLongOrigin = new LatLng(ambulance.getLatitude(), ambulance.getLongitude());
            origin[0] = latLongOrigin;
            Long distance = GoogleMapsUtil.getDistanceMatrix(origin, dest);
            if (distance == null) {
                distance = Long.MAX_VALUE;
            }
            ambulanceIdDistance.add(new Pair(ambulance, distance));
        }
        ambulanceIdDistance.sort(new Comparator<Pair>() {//ordena do menor pro maior
            @Override
            public int compare(Pair o1, Pair o2) {
                if ((Long) o1.getValue2() < (Long) o2.getValue2()) {
                    return -1;
                } else if ((Long) o1.getValue2() > (Long) o2.getValue2()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        return ambulanceIdDistance;
    }

    @Override
    public List<Incident> getIncidentsPendingDisptach() {
        return incidentDAO.retriveIncidentsPendingDisptach();
    }

    @Override
    public void dispatchAmbulances(Incident incident, List<Pair> selectedAmbulancesToBeDispatched) {
        List<AmbulanceSettings> ambulanceSettingsList = new ArrayList<>(incident.getAmbulanceSettings());
        for (Iterator<AmbulanceSettings> iterator = ambulanceSettingsList.iterator(); iterator.hasNext();) {
            AmbulanceSettings ambulanceSettings = iterator.next();
            try {
                for (Pair p : selectedAmbulancesToBeDispatched) {
                    Ambulance ambulance = (Ambulance) p.getValue1();
                    Station station = ambulance.getStation();

                    List<StaffFunction> availableEmployeesRequired = new ArrayList(ambulanceSettings.getStaffFunctions());
                    List<Employee> availableEmployeesAtStation = employeeDAO.retriveAvailableEmployeesAtStation(station, availableEmployeesRequired);
                    dispatchEmployees(ambulance, availableEmployeesRequired, availableEmployeesAtStation);

                    List<EquipmentType> availableEquipmentsRequired = new ArrayList(ambulanceSettings.getEquipmentTypes());
                    List<Equipment> availableEquipmentsAtStation = equipmentDAO.retriveAvailableEquipmentsAtStation(station, availableEquipmentsRequired);
                    dispatchEquipments(ambulance, availableEquipmentsRequired, availableEquipmentsAtStation);

                    Dispatch dispatch = new Dispatch();
                    dispatch.setAmbulance(ambulance);
                    dispatch.setDateTime(new Date());
                    dispatch.setIncident(incident);
                    incident.getDispatches().add(dispatch);
                    incident.setIncidentState(IncidentState.IN_ATTENDANCE);
                    ambulance.setAmbulanceState(AmbulanceState.DISPATCHED);
                    ambulanceDAO.save(ambulance);
                    dispatchDAO.save(dispatch);
                }
                iterator.remove();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        incidentDAO.save(incident);
    }

    private void dispatchEmployees(Ambulance ambulance, List<StaffFunction> employeesRequired, List<Employee> availableEmployeesAtStation) throws Exception {
        for (Employee availableEmployee : availableEmployeesAtStation) {
            for (Iterator<StaffFunction> iteratorStaffFucntion = employeesRequired.iterator(); iteratorStaffFucntion.hasNext();) {
                StaffFunction staffFunction = iteratorStaffFucntion.next();
                if (availableEmployee.getStaffFunctions().contains(staffFunction)) {
                    ambulance.getEmployees().add(availableEmployee);
                    availableEmployee.setAmbulance(ambulance);
                    iteratorStaffFucntion.remove();
                    employeeDAO.save(availableEmployee);
                }
            }
        }
        if (!employeesRequired.isEmpty()) {
            throw new Exception("Não existem funcionários suficientes");
        }
    }

    private void dispatchEquipments(Ambulance ambulance, List<EquipmentType> equipmentsRequired, List<Equipment> availableEquipmentsAtStation) throws Exception {
        for (Equipment availableEquipment : availableEquipmentsAtStation) {
            for (Iterator<EquipmentType> iteratorEquipmentType = equipmentsRequired.iterator(); iteratorEquipmentType.hasNext();) {
                EquipmentType equipmentType = iteratorEquipmentType.next();
                if (availableEquipment.getEquipmentType().equals(equipmentType)) {
                    ambulance.getEquipments().add(availableEquipment);
                    iteratorEquipmentType.remove();
                    availableEquipment.setAmbulance(ambulance);
                    equipmentDAO.save(availableEquipment);
                }
            }
        }
        if (!equipmentsRequired.isEmpty()) {
            throw new Exception("Não existem equipamentos suficientes");
        }
    }
}
