/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.persistence;

import br.ufes.inf.nemo.resgate.dispatch.domain.EmergencyCall;
import br.ufes.inf.nemo.resgate.dispatch.domain.EmergencyCall_;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

/**
 *
 * @author Thiago
 */
@Stateless
public class EmergencyCallJPADAO extends BaseJPADAO<EmergencyCall> implements EmergencyCallDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<EmergencyCall> getDomainClass() {
        return EmergencyCall.class;
    }

    @Override
    public List<EmergencyCall> getSimilarCalls(String description, String phoneNumber) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<EmergencyCall> cq = cb.createQuery(EmergencyCall.class);
        Root<EmergencyCall> root = cq.from(EmergencyCall.class);
        cq.where(cb.equal(root.get(EmergencyCall_.number), phoneNumber));
        cq.where(cb.or(cb.equal(root.get(EmergencyCall_.description), description)));
        List<EmergencyCall> emergencyCalls = null;
        try {
            emergencyCalls = entityManager.createQuery(cq).getResultList();
        } catch (RuntimeException e) {
        }
        return emergencyCalls;
    }
}
