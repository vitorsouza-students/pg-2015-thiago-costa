/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.domain;

import br.ufes.inf.nemo.resgate.data.domain.EquipmentType;
import br.ufes.inf.nemo.resgate.data.domain.StaffFunction;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;

/**
 *
 * @author Thiago
 */
@Entity
public class AmbulanceSettings extends PersistentObjectSupport implements Comparable<AmbulanceSettings> {

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<EquipmentType> equipmentTypes;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<StaffFunction> staffFunctions;

    public AmbulanceSettings() {
        equipmentTypes = new HashSet<>();
        staffFunctions = new HashSet<>();
    }

    public Set<EquipmentType> getEquipmentTypes() {
        return equipmentTypes;
    }

    public void setEquipmentTypes(Set<EquipmentType> equipmentTypes) {
        this.equipmentTypes = equipmentTypes;
    }

    public Set<StaffFunction> getStaffFunctions() {
        return staffFunctions;
    }

    public void setStaffFunctions(Set<StaffFunction> staffFunctions) {
        this.staffFunctions = staffFunctions;
    }

    @Override
    public int compareTo(AmbulanceSettings o) {
        if (this.staffFunctions.size() > o.staffFunctions.size()) {
            return 1;
        } else if (this.staffFunctions.size() < o.staffFunctions.size()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return staffFunctions + ", " + equipmentTypes;
    }
}
