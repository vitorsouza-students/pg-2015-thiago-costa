/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.domain;

/**
 *
 * @author thiago
 */
public enum AmbulanceState {

    INACTIVE, AT_STATION, DISPATCHED, EN_ROUTE, EN_ROUTE_TO_STATION, AT_HOSPITAL, AT_INCIDENT;
}
