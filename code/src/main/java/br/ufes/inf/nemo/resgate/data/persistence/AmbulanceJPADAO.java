/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.persistence;

import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.resgate.data.domain.AmbulanceState;
import br.ufes.inf.nemo.resgate.data.domain.Ambulance_;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author thiago
 */
@Stateless
public class AmbulanceJPADAO extends BaseJPADAO<Ambulance> implements AmbulanceDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<Ambulance> getDomainClass() {
        return Ambulance.class;
    }

    @Override
    public List<Ambulance> retrieveAllAmbulancesAvailableToDispatch() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Ambulance> cq = cb.createQuery(Ambulance.class);
        Root<Ambulance> root = cq.from(Ambulance.class);
        cq.where(cb.equal(root.get(Ambulance_.ambulanceState), AmbulanceState.EN_ROUTE_TO_STATION));
        cq.where(cb.or(cb.equal(root.get(Ambulance_.ambulanceState), AmbulanceState.AT_STATION)));
        List<Ambulance> ambulances = null;
        try {
            ambulances = entityManager.createQuery(cq).getResultList();
        } catch (RuntimeException e) {
        }
        return ambulances;
    }
}
