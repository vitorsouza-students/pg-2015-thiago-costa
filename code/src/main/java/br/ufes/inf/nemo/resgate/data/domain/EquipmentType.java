/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.domain;

/**
 *
 * @author thiago
 */
public enum EquipmentType {

    TROLLEY_BED, DEFIBRILLATOR, FIRST_AID_KIT;
}
