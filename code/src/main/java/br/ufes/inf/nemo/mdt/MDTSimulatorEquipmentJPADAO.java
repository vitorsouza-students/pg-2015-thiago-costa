/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.mdt;

import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.resgate.data.domain.Equipment;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Thiago
 */
@Stateless
public class MDTSimulatorEquipmentJPADAO extends BaseJPADAO<Equipment> implements MDTSimulatorEquipmentDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<Equipment> getDomainClass() {
        return Equipment.class;
    }

    @Override
    public void putEquipmentOnHold(Ambulance ambulance) {
        try {
            String jpql = "SELECT e FROM Equipment e where e.ambulance = :ambulance";

            TypedQuery<Equipment> query = entityManager.createQuery(jpql,
                    Equipment.class).setParameter("ambulance", ambulance);
            Equipment equipment = query.getSingleResult();
            equipment.setAmbulance(null);
            save(equipment);
        } catch (Exception ex) {

        }
    }
}
