/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.util;

import br.ufes.inf.nemo.resgate.data.application.ManageAmbulanceService;
import br.ufes.inf.nemo.resgate.data.domain.Ambulance;
import br.ufes.inf.nemo.resgate.data.domain.AmbulanceState;
import br.ufes.inf.nemo.resgate.data.domain.Employee;
import br.ufes.inf.nemo.resgate.data.domain.Equipment;
import br.ufes.inf.nemo.resgate.data.persistence.AmbulanceDAO;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.resgate.dispatch.domain.IncidentState;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;
import br.ufes.inf.nemo.util.ejb3.controller.PersistentObjectConverterFromId;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Thiago
 */
@Named
@ApplicationScoped
public class MdtSimulator extends CrudController<Ambulance> {

    @EJB
    private AmbulanceDAO ambulanceDAO;

    private Ambulance ambulance;

    private List ambulances;

    @EJB
    private ManageAmbulanceService manageAmbulanceService;

    private final MDTSimulatorAmbulanceDAO mDTSimulatorAmbulanceDAO = new MDTSimulatorAmbulanceDAO();

    private final MDTSimulatorIncidentDAO mDTSimulatorIncidentDAO = new MDTSimulatorIncidentDAO();

    private final MDTSimulatorEmployeeDAO mDTSimulatorEmployeeDAO = new MDTSimulatorEmployeeDAO();

    private final MDTSimulatorEquipmentDAO mDTSimulatorEquipmentDAO = new MDTSimulatorEquipmentDAO();

    public Ambulance getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(Ambulance ambulance) {
        this.ambulance = ambulance;
    }

    public Converter getAmbulanceConverter() {
        return new PersistentObjectConverterFromId<>(ambulanceDAO);
    }

    public List<Ambulance> getAmbulances() {
        if (ambulances == null) {
            ambulances = ambulanceDAO.retrieveAll();
        }
        return ambulances;
    }

    public void ambulanceLocalization() {
        mDTSimulatorAmbulanceDAO.save(ambulance);
    }

    public void confirmationIncidenteAttendance() {
        ambulance.setAmbulanceState(AmbulanceState.EN_ROUTE);
        mDTSimulatorAmbulanceDAO.save(ambulance);

        Incident incident = mDTSimulatorIncidentDAO.getIncidentByAmbulance();
        incident.setIncidentState(IncidentState.IN_ATTENDANCE);
        mDTSimulatorIncidentDAO.save(incident);
    }

    public void enRouteToHospital() {
        ambulance.setAmbulanceState(AmbulanceState.EN_ROUTE);
        mDTSimulatorAmbulanceDAO.save(ambulance);

        Incident incident = mDTSimulatorIncidentDAO.getIncidentByAmbulance();
        incident.setIncidentState(IncidentState.EN_ROUTE_TO_HOSPITAL);
        mDTSimulatorIncidentDAO.save(incident);
    }

    public void informAtHospital() {
        ambulance.setAmbulanceState(AmbulanceState.AT_HOSPITAL);
        mDTSimulatorAmbulanceDAO.save(ambulance);

        Incident incident = mDTSimulatorIncidentDAO.getIncidentByAmbulance();
        incident.setIncidentState(IncidentState.EN_ROUTE_TO_HOSPITAL);
        mDTSimulatorIncidentDAO.save(incident);
    }

    public void informEnRouteToStation() {
        ambulance.setAmbulanceState(AmbulanceState.EN_ROUTE_TO_STATION);
        mDTSimulatorAmbulanceDAO.save(ambulance);

        Incident incident = mDTSimulatorIncidentDAO.getIncidentByAmbulance();
        incident.setIncidentState(IncidentState.CLOSED);
        mDTSimulatorIncidentDAO.save(incident);
    }

    public void informOnHold() {
        ambulance.setAmbulanceState(AmbulanceState.AT_STATION);
        mDTSimulatorAmbulanceDAO.save(ambulance);
        mDTSimulatorEmployeeDAO.putEmployeesOnHold();
        mDTSimulatorEquipmentDAO.putEquipmentOnHold();
    }

    public void informProblem() {
        ambulance.setAmbulanceState(AmbulanceState.INACTIVE);
        mDTSimulatorAmbulanceDAO.save(ambulance);

        Incident incident = mDTSimulatorIncidentDAO.getIncidentByAmbulance();
        incident.setIncidentState(IncidentState.OPEN);
        mDTSimulatorIncidentDAO.save(incident);
    }

    @Override
    protected CrudService<Ambulance> getCrudService() {
        return manageAmbulanceService;
    }

    @Override
    protected Ambulance createNewEntity() {
        return new Ambulance();
    }

    @Override
    protected void initFilters() {

    }

    public class MDTSimulatorAmbulanceDAO extends BaseJPADAO<Ambulance> {

        @PersistenceContext
        private EntityManager entityManager;

        @Override
        protected EntityManager getEntityManager() {
            return entityManager;
        }

        @Override
        public Class<Ambulance> getDomainClass() {
            return Ambulance.class;
        }
    }

    public class MDTSimulatorIncidentDAO extends BaseJPADAO<Incident> {

        @PersistenceContext
        private EntityManager entityManager;

        @Override
        protected EntityManager getEntityManager() {
            return entityManager;
        }

        @Override
        public Class<Incident> getDomainClass() {
            return Incident.class;
        }

        public Incident getIncidentByAmbulance() {
            String jpql = "SELECT DISTINCT i FROM Incident in, Dispatch d where d in in.dispatches"
                    + " and d.ambulance = :ambulance";

            TypedQuery<Incident> query = entityManager.createQuery(jpql,
                    Incident.class).setParameter("ambulance", ambulance);
            Incident incident = query.getSingleResult();
            return incident;
        }
    }

    public class MDTSimulatorEmployeeDAO extends BaseJPADAO<Employee> {

        @PersistenceContext
        private EntityManager entityManager;

        @Override
        protected EntityManager getEntityManager() {
            return entityManager;
        }

        @Override
        public Class<Employee> getDomainClass() {
            return Employee.class;
        }

        public void putEmployeesOnHold() {
            String jpql = "SELECT e FROM Employee e where e.ambulance = :ambulance";

            TypedQuery<Employee> query = entityManager.createQuery(jpql,
                    Employee.class).setParameter("ambulance", ambulance);
            Employee employee = query.getSingleResult();
            employee.setAmbulance(null);
            mDTSimulatorEmployeeDAO.save(employee);
        }
    }

    public class MDTSimulatorEquipmentDAO extends BaseJPADAO<Equipment> {

        @PersistenceContext
        private EntityManager entityManager;

        @Override
        protected EntityManager getEntityManager() {
            return entityManager;
        }

        @Override
        public Class<Equipment> getDomainClass() {
            return Equipment.class;
        }

        public void putEquipmentOnHold() {
            String jpql = "SELECT e FROM Equipment e where e.ambulance = :ambulance";

            TypedQuery<Equipment> query = entityManager.createQuery(jpql,
                    Equipment.class).setParameter("ambulance", ambulance);
            Equipment equipment = query.getSingleResult();
            equipment.setAmbulance(null);
            mDTSimulatorEquipmentDAO.save(equipment);
        }
    }
}
