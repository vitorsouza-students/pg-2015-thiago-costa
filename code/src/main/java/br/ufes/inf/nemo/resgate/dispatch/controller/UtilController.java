/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.dispatch.controller;

import br.ufes.inf.nemo.resgate.data.domain.EquipmentType;
import br.ufes.inf.nemo.resgate.data.domain.Hospital;
import br.ufes.inf.nemo.resgate.data.domain.StaffFunction;
import br.ufes.inf.nemo.resgate.data.persistence.HospitalDAO;
import br.ufes.inf.nemo.resgate.dispatch.domain.Incident;
import br.ufes.inf.nemo.resgate.dispatch.domain.IncidentState;
import br.ufes.inf.nemo.resgate.dispatch.domain.PriorityLevel;
import br.ufes.inf.nemo.resgate.dispatch.persistence.IncidentDAO;
import br.ufes.inf.nemo.util.ejb3.controller.PersistentObjectConverterFromId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.convert.Converter;
import javax.inject.Named;

/**
 *
 * @author Thiago
 */
@Named
@ApplicationScoped
public class UtilController {

    private List<StaffFunction> staffFunctions;

    private List<EquipmentType> equipmentTypes;

    private List<PriorityLevel> priorityLevels;

    private List<IncidentState> incidentStates;

    private List<Hospital> hospitals;

    @EJB
    private HospitalDAO hospitalDAO;

    @EJB
    private IncidentDAO incidentDAO;

    private PersistentObjectConverterFromId<Hospital> hospitalConverter;

    private PersistentObjectConverterFromId<Incident> incidentConverter;

    public List<StaffFunction> getStaffFunctions() {
        if (staffFunctions == null) {
            staffFunctions = new ArrayList<>();
            staffFunctions.addAll(Arrays.asList(StaffFunction.values()));
        }
        return staffFunctions;
    }

    public void setStaffFunctions(List<StaffFunction> staffFunctions) {
        this.staffFunctions = staffFunctions;
    }

    public List<EquipmentType> getEquipmentTypes() {
        if (equipmentTypes == null) {
            equipmentTypes = new ArrayList<>();
            equipmentTypes.addAll(Arrays.asList(EquipmentType.values()));
        }
        return equipmentTypes;
    }

    public void setEquipmentTypes(List<EquipmentType> equipmentTypes) {
        this.equipmentTypes = equipmentTypes;
    }

    public List<PriorityLevel> getPriorityLevels() {
        if (priorityLevels == null) {
            priorityLevels = new ArrayList<>();
            priorityLevels.addAll(Arrays.asList(PriorityLevel.values()));
        }
        return priorityLevels;
    }

    public void setPriorityLevels(List<PriorityLevel> priorityLevels) {
        this.priorityLevels = priorityLevels;
    }

    public Converter getHospitalController() {
        if (hospitalConverter == null) {
            hospitalConverter = new PersistentObjectConverterFromId<>(hospitalDAO);
        }
        return hospitalConverter;
    }

    public Converter getIndentConverter() {
        if (incidentConverter == null) {
            incidentConverter = new PersistentObjectConverterFromId<>(incidentDAO);
        }
        return incidentConverter;
    }

    public List<Hospital> getHospitals() {
        if (hospitals == null) {
            hospitals = hospitalDAO.retrieveAllActiveHospitals();
        }
        return hospitals;
    }

    public List<IncidentState> getIncidentStates() {
        if (incidentStates == null) {
            incidentStates = new ArrayList<>();
            incidentStates.addAll(Arrays.asList(IncidentState.values()));
        }
        return incidentStates;
    }

    public void setIncidentStates(List<IncidentState> incidentStates) {
        this.incidentStates = incidentStates;
    }

}
