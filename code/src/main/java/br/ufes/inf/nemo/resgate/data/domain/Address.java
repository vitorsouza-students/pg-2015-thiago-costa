/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.inf.nemo.resgate.data.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

/**
 *
 * @author Thiago
 */
@Entity
public class Address extends PersistentObjectSupport implements Comparable<Address> {

    @Size(max = 150)
    private String street;

    @Size(max = 10)
    private String zipCode;

    @Size(max = 50)
    private String neighborhood;

    @Size(max = 50)
    private String city;

    private double latitude;

    private double longitude;

    public String getStreet() {
        return street;
    }

    public void setStreet(String stree) {
        this.street = stree;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int compareTo(Address o) {
        return 0;
    }
}
