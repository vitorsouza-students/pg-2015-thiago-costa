# README #

Projeto de Graduação de Thiago Martinho da Costa: _Resgate - Despacho de ambulância automatizado_.

### Resumo ###

Na década de 1990, um sistema de despacho de ambulâncias foi encomendado para a capital da Inglaterra, Londres, devido a necessidade de uma nova forma de gerenciar o controle de ambulâncias para a cidade. No primeiro momento a ideia foi implantada de forma errônea e a falha do sistema London Ambulance Computer-Aided Dispatch (LAS-CAD), relatada inicialmente em (FINKELSTEIN, 1993), se tornou um caso muito famoso de estudo dentro da área de Engenharia de Software, conforme declarado por (SOUZA; MYLOPOULOS, 2013).

Em (SOUZA, 2007), foi proposto o método FrameWeb - Framework-based Design Mehtod for Web Engineering que sugere a utilização de vários frameworks afim de obter uma cons- trução mais rápida e eficiente do sistema além de propor uma arquitetura de software dada através de modelos contendo informações relevantes para desenvolvimento de ferramentas voltada para a web.

Este projeto, portanto, visa modelar, projetar e desenvolver a base desse sistema de ambulâncias para que, no futuro, possa ser evoluído para uma versão adaptativa de modo a ser utilizado nas pesquisas dessa área, além de prover uma implementação para um sistema que é bastante utilizado como exemplo em pesquisas, facilitando, assim, futuros testes nesta área. Outro objetivo consiste em experimentar o framework FrameWeb, provendo feedback para esta pesquisa em andamento.

Baseando-se no FrameWeb e partindo dos requisitos elicitados em (SOUZA; MYLOPOU- LOS, 2013), foi desenvolvido um sistema, na plataforma JavaTM EE 7, de despacho de ambulâncias automatizado chamado Resgate que utiliza diversos frameworks e padrões de desenvolvimento voltados para a web, como por exemplo, JavaServer Faces (JSF), Contexts Depedency Injection (CDI) e Java Persistence API.